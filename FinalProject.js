//Project Idea: What time zone am I in and what time is it? 

/**
 * Validates Zip Code Input
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */

const regEx = /^\d{5}(?:[-\s]\d{4})?$/ //zip code Regex


const validateZip = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

    if (regEx.test(inputEl.value) === false) {

      errorEl.innerHTML = `Zip Code is Invalid`;

    // Prevent form submit
    submitEvent.preventDefault();

  } else {
    errorEl.innerHTML = '';
    errorEl.classList.remove('d-block');
  }
}

const inputElements = document.getElementsByClassName('validate-input');
const formEl = document.getElementById('connect-form');
const zip = document.getElementById('zipInput');

formEl.addEventListener('submit', function(e) {
  
  validateZip(zip, e);
  e.preventDefault();
  
//zip.value is stored value of zip code input //why can I only pass this in, in the function. 

//API 1: Take zip code input and define long and lat API

  var API_KEY = `AIzaSyDc3-0JLZ0jdX9hDfCkPLPlHV0L9B9qAT4`
  var BASE_URL1 = `https://maps.googleapis.com/maps/api/geocode/json?address=${zip.value}&key=`
  var url1 = `${BASE_URL1}${API_KEY}`; //modify this to obtain intended results

    fetch(url1)
      .then(function(response) {
        return response.json(); 
      })

      .then(function(responseJson) {
           
            let loc = {
              lat: responseJson.results[0].geometry.location.lat,
              long: responseJson.results[0].geometry.location.lng,
              name: responseJson.results[0].address_components[1].long_name,
            } //constructor function

          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: loc.lat, lng: loc.long},
            zoom: 6
          });
          infoWindow = new google.maps.InfoWindow;

        getTimeZone(API_KEY,loc.lat,loc.long,loc.name); //execution of api 2 and 3
      });
        });   


//API 2: to take long and lat and define time zone
  var getTimeZone = function(api,lat,long,location) {
    var BASE_URL2 = `https://maps.googleapis.com/maps/api/timezone/json?location=${lat},${long}&timestamp=1553577702&key=${api}`
      var url2 = `${BASE_URL2}`

      fetch(url2)
      .then(function(response) {
          return response.json(); 
      })
      //The local time of a given location is the sum of the timestamp parameter, and the dstOffset and rawOffset fields from the result.

      .then(function(responseJson) {
        var timeZoneId = responseJson.timeZoneId;
        var timeZoneName = responseJson.timeZoneName;
       
        var dst = (responseJson.dstOffset/3600);
        console.log(dst)
        var raw = (responseJson.rawOffset/3600);
        console.log(raw)
        const now = new Date();
        console.log(now.getUTCHours())
        // Google give you the timezone offset in seconds.  Calculate how many hours the time is offset from UTC
        const hourOffset = raw + dst // do calculation based on raw offset & daylight savings
        let hour = now.getUTCHours() + hourOffset;
        if( hour<0){
          hour = 24+now.getUTCHours()+raw+dst
        }
        
        
        // check if this is negative, and do something if it is
        // i.e. if UTC time is 2 AM and offset is -7 hours, you would not want to show -5
        
        
        // If the offset isn't by an exact number of hours, calculate the minute offset as well
        const minutesOffset = dst+raw// do calculation based on raw offset & daylight savings
        let minutes = ('0' + now.getUTCMinutes()).slice(-2) //+ minutesOffset;
        
        // check if this is negative, and do something if it is

          document.getElementById(`time`).innerHTML = `${location} is located in the "${timeZoneName} Zone" and the time is currently ${hour}:${minutes}`
          
    });
  };

//Change Background Color Timing Function 
    
let C1 = 118;
let C2 = 200
let C3 = 220
let a = document.getElementById('background');

const changeB = function() {
    
    if(a.style.backgroundColor === `rgb(255, 255, 255)`) {
        return;     
    };
    if(a.style.backgroundColor != `rgb(255, 255, 255)`) {
         //execute color change to white 
         C1 = C1+.5;
         C2 = C2+.5;
         C3 = C3 +.5;
         a.style.backgroundColor = `rgb(${C1},${C2},${C3})`;
        requestAnimationFrame(changeB)};
    };
    requestAnimationFrame(changeB);


   
    